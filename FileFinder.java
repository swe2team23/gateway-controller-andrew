package gatewayController;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;


public class FileFinder {
    private Path path;
    private Path realPath;

    //constructor creates filepath based on the name of the file passed to it
    FileFinder(String filename) throws IOException {
        path = Paths.get(filename);
        realPath = path.toRealPath(LinkOption.NOFOLLOW_LINKS);
    }

    //returns filepath
    public Path getPath() {
        return realPath;
    }

}

