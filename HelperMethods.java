package gatewayController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HelperMethods {

    public static String getTimeStamp() throws IOException, InterruptedException {

        FileFinder file = new FileFinder("heartbeat.py");
        String filepath = file.getPath().toString();
        String runString = ("cmd /c "+filepath);

        Runtime rt2 = Runtime.getRuntime();
        Process pr2 = rt2.exec(runString);
        pr2.waitFor();

        BufferedReader br = new BufferedReader(new InputStreamReader(pr2.getInputStream()));

        String timestamp = br.readLine();
        return timestamp;
    }
}
