package gatewayController;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

//import com.google.gson.Gson;


public class HttpRequests {

    public static StringBuffer httpPost(String urlString, String urlParameters, int gwID) {

        String url = urlString;

        URL obj = null;
        DataOutputStream outStream = null;
        HttpsURLConnection connection = null;
        String jsonString = "";

        BufferedReader reader = null;

        //heartbeat to be posted to the REST API
        jsonString = "{\"gw_uuid\": \""+gwID+"\", \"timestamp\": \""+urlParameters+"\", \"status\": \""+"OK"+"\"}";

        //instantiate URL object
        try {
            obj = new URL(url);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //open connection
        try {
            connection = (HttpsURLConnection) obj.openConnection();
            connection.addRequestProperty("Content-Type", "application/json");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //set request method to POST
        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        connection.setDoOutput(true); // Triggers POST.
        try {
            connection.connect();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            outStream = new DataOutputStream(connection.getOutputStream());
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try {
            outStream.writeBytes(jsonString);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            outStream.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            outStream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            int responseCode = connection.getResponseCode();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        try {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String input;
        StringBuffer response = new StringBuffer();

        //gets response back from server
        try {
            while ((input = reader.readLine()) != null) {
                response.append(input);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //System.out.println(response);
        try {
            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //return value is response from server
        return response;

    }

    public static StringBuffer httpPostMemory(String urlString, String urlParameters, int gwID) throws IOException, InterruptedException {

        String url = urlString;

        URL obj = null;
        DataOutputStream outStream = null;
        HttpsURLConnection connection = null;
        String jsonString = "";
        String timestamp = HelperMethods.getTimeStamp();
        BufferedReader reader = null;

        //memory test result to be posted to the REST API
        jsonString = "{\"gw_uuid\": \""+gwID+"\", \"result\": \""+urlParameters+"\", \"timestamp\": \""+timestamp+"\"}";

        System.out.println(jsonString);

        try {
            obj = new URL(url);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        try {
            connection = (HttpsURLConnection) obj.openConnection();
            connection.addRequestProperty("Content-Type", "application/json");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        connection.setDoOutput(true); // Triggers POST.

        try {
            connection.connect();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            outStream = new DataOutputStream(connection.getOutputStream());
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try {

            outStream.writeBytes(jsonString);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            outStream.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            outStream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            int responseCode = connection.getResponseCode();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        try {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String input;
        StringBuffer response = new StringBuffer();

        try {
            while ((input = reader.readLine()) != null) {
                response.append(input);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return response;

    }

}
