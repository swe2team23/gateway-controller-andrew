package gatewayController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Loop implements Runnable {
    public void run()
    {
        try
        {

            String response = gatewayController.PythonReader.sendHeartbeat(); //response from cic server after posting a heartbeat
            System.out.println("response from server: "+response);
            if(response.equals("[]")) {
                //no on demand diagnostic queued up, do nothing
            }
            else {
                String filename = Loop.parseResponse(response); //parseResponse method returns the filename to be run

                System.out.println("parsed resonse: " + filename);

                if (filename.equals("memory.py")) {
                    Thread object = new Thread(new MemoryTest());
                    object.start();
                }
                if (filename.equals("cpu.py")){
                    Thread object = new Thread(new CpuTest());
                    object.start();
                }
            }


        }
        catch (Exception e)
        {
            // Throwing an exception
            System.out.println ("Exception is caught");
            e.printStackTrace();
        }
    }

    //Receives json string
    //Returns filename
    public static String parseResponse(String response){
        response = response.substring(1, response.length()-1);

        JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();

        String filename = jsonObject.get("filename").getAsString();

        return filename;


    }
}

class MemoryTest implements Runnable
{
    public void run()
    {
        try
        {
            String response = gatewayController.PythonReader.sendMemory();
            //System.out.println(response);

        }
        catch (Exception e)
        {
            // Throwing an exception
            System.out.println ("Exception is caught");
        }
    }
}

class CpuTest implements Runnable
{
    public void run()
    {
        try
        {
            //String response = softwareEngineering.PythonReader.sendCpu();
            //System.out.println(response);

        }
        catch (Exception e)
        {
            // Throwing an exception
            System.out.println ("Exception is caught");
        }
    }
}


