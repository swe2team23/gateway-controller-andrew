package gatewayController;
import java.io.*;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.file.Path;

import javax.net.ssl.HttpsURLConnection;


public class PythonReader {
    /**
     * @param args
     * @throws IOException
     */

    public static String sendHeartbeat() {
        String returnval = "";

        try {
            int gwID = 1;

            //String url = "https://team23.staging.softwareengineeringii.com/api/heartbeat";
            String url = "https://team23.dev.softwareengineeringii.com/api/heartbeat";

            FileFinder file = new FileFinder("heartbeat.py");
            String filepath = file.getPath().toString();
            String runString = ("cmd /c "+filepath); //constructs string used to run python script

            Runtime rt2 = Runtime.getRuntime();
            Process pr2 = rt2.exec(runString);

            pr2.waitFor();

            BufferedReader br = new BufferedReader(new InputStreamReader(pr2.getInputStream()));

            String heartbeat = br.readLine();

            StringBuffer response = HttpRequests.httpPost(url, heartbeat, gwID);

            returnval = response.toString();

        }
        catch(Exception e)
        {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return returnval;
    }

    public static String sendMemory() {
        String returnval = "";

        try {
            //String url = "https://team23.staging.softwareengineeringii.com/api/memoryTest";
            String url = "https://team23.dev.softwareengineeringii.com/api/memoryTest";

            int gwID = 1;

            FileFinder file = new FileFinder("memory.py");
            String filepath = file.getPath().toString();
            String runString = ("cmd /c "+filepath);

            Runtime rt2 = Runtime.getRuntime();
            Process pr2 = rt2.exec(runString);
            pr2.waitFor();

            BufferedReader br = new BufferedReader(new InputStreamReader(pr2.getInputStream()));

            String memory = br.readLine();

            StringBuffer response = HttpRequests.httpPostMemory(url, memory, gwID);
            returnval = response.toString();

        }
        catch(Exception e)
        {
            System.out.println(e.toString());
            e.printStackTrace();
        }

        return returnval;
    }

}



