package gatewayController;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class TestCases {



    @Test
    public void sendHeartbeatTest() {
        String response = PythonReader.sendHeartbeat();
        String expectedResponse = "{\"status\":\"OK\"}";
        assert response == expectedResponse : "The response is not as expected " + response;
    }

    @Test
    public void sendMemoryTest() {
        String response = PythonReader.sendMemory();
        String expectedResponse = "{\"status\":\"OK\"}";
        assert response == expectedResponse : "The response is not as expected " + response;
    }

    @Test
    public void filePathTest() throws IOException {
        FileFinder findFile = new FileFinder("heartbeat.py");
        String filepath = findFile.getPath().toString();
        assert filepath.equals("C:\\Users\\Lenovo\\eclipse-workspace\\software_engineering\\heartbeat.py"): "Filepath is not correct";
    }

    @Test
    public void jsonParserTest() {
        String filename = Loop.parseResponse("[{\"filename\":\"OK.txt\"}]");
        assert filename.equals("OK.txt"):"parser response was not as expected";
    }


}